<?php

declare(strict_types=1);

namespace Dexodus\TextCaseBundle\DependencyInjection\Compiler;

use Dexodus\TextCaseBundle\Service\WordsExtractor\WordsExtractor;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class LoadExtractorsCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        $wordsExtractorDefinition = $container->findDefinition(WordsExtractor::class);
        $wordsExtractorClasses = $container->findTaggedServiceIds('text-case.words-extractor');

        $wordsExtractorDefinition->addMethodCall('setExtractors', [$wordsExtractorClasses]);
    }
}
