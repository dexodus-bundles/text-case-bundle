<?php

declare(strict_types=1);

namespace Dexodus\TextCaseBundle;

use Dexodus\TextCaseBundle\DependencyInjection\Compiler\LoadExtractorsCompilerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class TextCaseBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        $container->addCompilerPass(new LoadExtractorsCompilerPass());

        parent::build($container);
    }
}
