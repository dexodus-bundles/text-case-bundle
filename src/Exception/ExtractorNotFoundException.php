<?php

declare(strict_types=1);

namespace Dexodus\TextCaseBundle\Exception;

use Dexodus\TextCaseBundle\Enum\TextCaseEnum;
use Exception;

class ExtractorNotFoundException extends Exception
{
    public function __construct(TextCaseEnum $extractorCase, array $availableExtractorsClassnames)
    {
        $availableExtractors = implode(', ', $availableExtractorsClassnames);
        $message = "Not found extractor for case '$extractorCase->value'. Available extractors: [$availableExtractors]";

        parent::__construct($message);
    }
}
