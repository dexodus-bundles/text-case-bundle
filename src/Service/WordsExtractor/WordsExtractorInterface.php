<?php

declare(strict_types=1);

namespace Dexodus\TextCaseBundle\Service\WordsExtractor;

interface WordsExtractorInterface
{
    public function extract(string $string): array;
}
