<?php

declare(strict_types=1);

namespace Dexodus\TextCaseBundle\Service\WordsExtractor;

use Dexodus\TextCaseBundle\Enum\TextCaseEnum;
use Dexodus\TextCaseBundle\Exception\ExtractorNotFoundException;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\DependencyInjection\ContainerInterface;

class WordsExtractor
{
    /** @var string[] */
    private array $extractorClasses = [];

    /** @var WordsExtractorInterface[] */
    private array $extractors = [];

    public function __construct(
        #[Autowire(service: 'service_container')] private readonly ContainerInterface $container,
    ) {
    }

    /** @param string[] $extractorClasses */
    public function setExtractors(array $extractorClasses): void
    {
        foreach ($extractorClasses as $extractorClass => $empty) {
            $extractorClassnameParts = explode('\\', $extractorClass);
            $extractorClassname = end($extractorClassnameParts);
            $this->extractorClasses[$extractorClassname] = $extractorClass;
        }
    }

    public function extract(string $str, TextCaseEnum $inputCase): array
    {
        return $this->getExtractor($inputCase)->extract($str);
    }

    private function getExtractor(TextCaseEnum $inputCase): WordsExtractorInterface
    {
        $extractorClassName = $inputCase->value . 'WordsExtractor';
        $extractorClass = $this->extractorClasses[$extractorClassName] ?? null;

        if (is_null($extractorClass)) {
            throw new ExtractorNotFoundException($inputCase, array_keys($this->extractorClasses));
        }

        if (!array_key_exists($extractorClass, $this->extractors)) {
            $this->extractors[$extractorClass] = $this->container->get($extractorClass);
        }

        return $this->extractors[$extractorClass];
    }
}
