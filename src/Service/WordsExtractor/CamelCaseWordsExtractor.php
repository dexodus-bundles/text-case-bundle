<?php

declare(strict_types=1);

namespace Dexodus\TextCaseBundle\Service\WordsExtractor;

class CamelCaseWordsExtractor implements WordsExtractorInterface
{
    public function extract(string $string): array
    {
        $words = [];
        $currentWord = '';

        for ($i = 0; $i < strlen($string); $i++) {
            $char = $string[$i];

            if (preg_match('~[A-ZА-Я]~', $char)) {
                if ($currentWord !== '') {
                    $words[] = $currentWord;
                }

                $currentWord = '';
            }

            $currentWord .= $char;
        }

        if ($currentWord !== '') {
            $words[] = $currentWord;
        }

        return $words;
    }
}
