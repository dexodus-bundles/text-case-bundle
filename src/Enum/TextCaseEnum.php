<?php

declare(strict_types=1);

namespace Dexodus\TextCaseBundle\Enum;

enum TextCaseEnum: string
{
    // Example: "itsCamelCase"
    case CAMEL_CASE = 'CamelCase';

    // Example: "its_snake_case"
    case SNAKE_CASE = 'SnakeCase';
}
